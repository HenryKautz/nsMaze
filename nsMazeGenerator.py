from __future__ import annotations
from typing import *
import random
import numpy as np

#     0 1 2 3 4 5 6
#  0  @ @ @ @ @ @ @
#  1  @ . * . * . @
#  2  @ * @ * @ * @
#  3  @ . * . * . @
#  4  @ @ @ @ @ @ @
#
#  @ = fixed wall
#  * = non-fixed wall
#  . = space
#
#  width    center      //2
#  3        1           1
#  5        1 or 3      2
#  7        3           3
#  9        3 or 5      4
#  11       5           5
#  Formula is n//2 + (n//2+1)%2


def even(n: int) -> bool:
    return (n % 2) == 0


def odd(n: int) -> bool:
    return not even(n)


class Cell:
    def __init__(self, r: int, c: int):
        self.r = r
        self.c = c
        self.visited = False
        self.reset()

    def reset(self) -> None:
        self.isWall = even(self.r) or even(self.c)
        self.fixed = even(self.r) and even(self.c)
        self.parent = self
        self.visited = False

    def visit(self) -> None:
        if self.visited:
            raise NameError("Bad call to Cell.visit at " +
                            str(self.r) + "," + str(self.c))
        self.visited = True

    def knock(self) -> None:
        if (self.fixed):
            raise NameError("Bad call to Cell.knock at " +
                            str(self.r) + "," + str(self.c))
        self.isWall = False

    def find(self) -> Cell:
        if (self.parent is not self):
            self.parent = self.parent.find()
        return self.parent

    def union(self, other: Cell) -> Cell:
        c1 = self.find()
        c2 = other.find()
        c1.parent = c2


class nsMazeGenerator:
    def __init__(self, height: int, width: int):
        if even(height) or even(width):
            raise NameError("Error, height and width must be odd")
        self.board = []
        self.width = width
        self.height = height
        for i in range(height):
            self.board.append([])
            for j in range(width):
                self.board[i].append(Cell(i, j))

    def toList(self) -> List[List[Cell]]:
        a = []
        for i in range(self.height):
            a.append([])
            for j in range(self.width):
                c: Cell = self.board[i][j]
                n: int
                if c.fixed:
                    n = 1
                elif c.isWall:
                    n = 1
                else:
                    n = 0
                a[i].append(n)
        return a

    def toArray(self):
        return np.array(self.toList())

    def toStr(self) -> str:
        return "\n".join([" ".join([(" " if c == 0 else "#")
                                    for c in r]) for r in self.toList()])

    def reset(self) -> None:
        for i in range(self.height):
            for j in range(self.width):
                self.board[i][j].reset()

    def create(self, creator: str = 'GrowingTree', startcell='center', cycles=0, randomness=0) -> None:
        # Create exits
        self.entranceRow = random.randrange(1, self.height-1, 2)
        self.exitRow = random.randrange(1, self.height-1, 2)

        if (creator == 'Kruskals'):
            self.createKruskals()
        elif (creator == 'GrowingTree'):
            self.createGrowingTree(randomness=randomness, startcell=startcell)
        else:
            raise NameError("Bad call to nsMaze.solve()")

        self.board[self.entranceRow][0].knock()
        self.board[self.exitRow][self.width-1].knock()

        # Create cycles
        walls: List[Cell] = []
        for i in range(1, self.height-1):
            for j in range(1, self.width-1):
                c: Cell = self.board[i][j]
                if c.isWall and not c.fixed:
                    walls.append(c)
        random.shuffle(walls)
        for k in range(cycles):
            walls[k].knock()

    def createGrowingTree(self, randomness: int, startcell='center') -> None:
        # Start with a center space
        start: Cell
        if startcell == 'center':
            start = self.board[self.height//2 + (self.height//2+1) % 2][
                self.width//2 + (self.width//2 + 1) % 2]
        elif startcell == 'entrance':
            start = self.board[self.entranceRow][1]
        elif startcell == 'exit':
            start = self.board[self.exitRow][self.width-2]
        else:
            raise NameError('Bad starting cell specified ', startcell)
        toExplore: List[Cell] = [start]
        start.visit()
        while len(toExplore) > 0:
            space: Cell
            if random.randrange(0, 100) >= randomness:
                # Pick most recent
                space = toExplore[-1]
            else:
                # pick randomly
                space = random.choice(toExplore)
            if space.isWall:
                raise NameError("Error wall in toVisit at ",
                                space.r, " ", space.c)
            # create list of adjacent spaces
            adjacent = []
            if space.r > 2 and not self.board[space.r-2][space.c].visited:  # Up
                adjacent.append(self.board[space.r-2][space.c])
            # Down
            if space.r < self.height-2 and not self.board[space.r+2][space.c].visited:
                adjacent.append(self.board[space.r+2][space.c])
            if space.c > 2 and not self.board[space.r][space.c-2].visited:  # Left
                adjacent.append(self.board[space.r][space.c-2])
            # Right
            if space.c < self.width-2 and not self.board[space.r][space.c+2].visited:
                adjacent.append(self.board[space.r][space.c+2])
            if len(adjacent) > 0:
                neighbor: Cell = random.choice(adjacent)
                # determine the wall to knock down
                wr: int = (space.r + neighbor.r)//2
                wc: int = (space.c + neighbor.c)//2
                self.board[wr][wc].knock()
                # add neighbor to cells to explore
                toExplore.append(neighbor)
                neighbor.visit()
            else:
                # remove cell from toExplore
                toExplore.remove(space)

    def createKruskals(self) -> None:
        # Create a shuffled list of non-fixed walls
        walls = []
        for i in range(1, self.height-1):
            for j in range(1, self.width-1):
                if self.board[i][j].isWall and not self.board[i][j].fixed:
                    walls.append(self.board[i][j])
        random.shuffle(walls)
        # For each wall, if cells are in different sets, knocks down wall and union
        w: Cell
        for w in walls:
            # determine which cells are on either side of the wall
            if even(w.r):
                # vertical corridor
                c1 = self.board[w.r - 1][w.c]
                c2 = self.board[w.r + 1][w.c]
            else:
                # horizontal corridor
                c1 = self.board[w.r][w.c - 1]
                c2 = self.board[w.r][w.c + 1]
            f1 = c1.find()
            f2 = c2.find()
            if (f1 is not f2):
                w.knock()
                f1.union(f2)


def tester(r: int, c: int, cycles: int, creator: str, randomness: int, startcell: str):
    m = nsMazeGenerator(r, c)
    m.create(creator=creator, cycles=cycles,
             randomness=randomness, startcell=startcell)
    print(m.toStr())


if __name__ == "__main__":
    import sys
    if len(sys.argv) < 7:
        print("Use: python nsMaze height width cycles creator randomness startcell")
        exit()
    tester(int(sys.argv[1]), int(sys.argv[2]),
           int(sys.argv[3]),  sys.argv[4], int(sys.argv[5]), sys.argv[6])
