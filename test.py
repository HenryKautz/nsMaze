from nsMazeGenerator import *
from nsMazeSolver import *


def test(rows: int, cols: int):
    print("Generating maze")
    m = nsMazeGenerator(rows, cols)
    m.create(creator='GrowingTree', cycles=0,
             randomness=25, startcell="exit")
    print(m.toStr())
    mazeList = m.toList()
    s = nsMazeSolver(mazeList)
    print('Result of solving: ', s.solve())
    print(s.toStr())


if __name__ == "__main__":
    import sys
    if len(sys.argv) < 3:
        print("Use: python test.py height width")
        exit()
    test(int(sys.argv[1]), int(sys.argv[2]))
