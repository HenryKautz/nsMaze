from __future__ import annotations
from typing import *
import numpy as np
import heapdict

#     0 1 2 3 4 5 6
#  0  @ @ @ @ @ @ @
#  1  @ . * . * . @
#  2  @ * @ * @ * @
#  3  @ . * . * . @
#  4  @ @ @ @ @ @ @
#
#  @ = fixed wall
#  * = non-fixed wall
#  . = space

INFINITY: int = 1000000000


def even(n: int) -> bool:
    return (n % 2) == 0


def odd(n: int) -> bool:
    return not even(n)


class Cell:
    def __init__(self, r: int, c: int, isWall: bool):
        self.r = r
        self.c = c
        self.isWall = isWall
        self.reset()

    def reset(self) -> None:
        self.parent = self
        self.closed = False
        self.marked = False
        self.distFromStart = INFINITY

    def heuristic(self, goal: Cell) -> int:
        return self.distFromStart + abs(self.r - goal.r) + abs(self.c - goal.c)


class nsMazeSolver:
    def __init__(self, mazeList: List[List[int]]):
        self.height = len(mazeList)
        self.width = len(mazeList[0])
        self.board: List[List[Cell]] = []
        for i in range(self.height):
            self.board.append([])
            for j in range(self.width):
                self.board[i].append(Cell(i, j, mazeList[i][j] > 0))
        for i in range(self.height):
            if not self.board[i][0].isWall:
                self.entrance: Cell = self.board[i][0]
            if not self.board[i][self.width-1].isWall:
                self.exit: Cell = self.board[i][self.width-1]

    def toList(self) -> List[List[int]]:
        a: List[List[int]] = []
        for i in range(self.height):
            a.append([])
            for j in range(self.width):
                c: Cell = self.board[i][j]
                n: int
                if c.marked:
                    n = 2
                elif c.isWall:
                    n = 1
                else:
                    n = 0
                a[i].append(n)
        return a

    def toArray(self):
        return np.array(self.toList())

    def toStr(self) -> str:
        return "\n".join([" ".join([(" " if c == 0 else
                                     ("#" if c == 1 else "*"))
                                    for c in r]) for r in self.toList()])

    def reset(self) -> None:
        for i in range(self.height):
            for j in range(self.width):
                self.board[i][j].reset()

    def neighbors(self, space: Cell) -> List[Cell]:
        adjacent = []
        if space.r > 0:
            adjacent.append(self.board[space.r-1][space.c])
        if space.r < self.height-1:
            adjacent.append(self.board[space.r+1][space.c])
        if space.c > 0:
            adjacent.append(self.board[space.r][space.c-1])
        if space.c < self.width-1:
            adjacent.append(self.board[space.r][space.c+1])
        return adjacent

    def solve(self) -> bool:
        self.entrance.distFromStart = 0
        toExplore: heapdict.heapdict = heapdict.heapdict()
        toExplore[self.entrance] = self.entrance.heuristic(self.exit)
        while len(toExplore) > 0:
            cell: Cell
            (cell, _) = toExplore.popitem()
            if cell is self.exit:
                break
            if cell.closed:
                continue  # Should not actually occur!
            cell.closed = True
            for neighbor in self.neighbors(cell):
                if neighbor.isWall or neighbor.closed:
                    continue
                if neighbor.distFromStart > cell.distFromStart + 1:
                    neighbor.distFromStart = cell.distFromStart + 1
                    neighbor.parent = cell
                    toExplore[neighbor] = neighbor.heuristic(self.exit)
        if self.exit.distFromStart == INFINITY:
            return False
        cell = self.exit
        while True:
            cell.marked = True
            if cell is self.entrance:
                break
            cell = cell.parent
        return True
